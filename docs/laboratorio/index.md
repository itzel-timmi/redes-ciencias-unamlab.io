---
title: Laboratorio
---

# Laboratorio

## Prácticas

| Título                                                                                  | Ambiente       | Entrega
|:----------------------------------------------------------------------------------------|:--------------:|:----------:|
| **Práctica  1**: [Configuración de _switches_ administrables](practica-1)               | Packet Tracer  | _pendiente_
| **Práctica  2**: [Configuración de VLAN en _switches_ administrables](practica-2)       | Packet Tracer  | _pendiente_
| **Práctica  3**: [Configuración de _routers_](practica-3)                               | Packet Tracer  | _pendiente_
| **Práctica  4**: [Configuración de algoritmos de ruteo](practica-4)                     | Packet Tracer  | _pendiente_
| **Práctica  5**: [Configuración de servicios DHCP, NAT y DNS en GNU/Linux](practica-5)  | VirtualBox     | _pendiente_
| **Práctica  6**: [Implementación de un servidor de archivo con NFS y Samba](practica-6) | VirtualBox     | _pendiente_
| **Práctica  7**: [Configuración de un _firewall_ de red con pfSense](practica-7)        | VirtualBox     | _pendiente_
| **Práctica  8**: [Contenedores Linux con Docker](practica-8)                            | VirtualBox     | _pendiente_
| **Práctica  9**: [Registro de dominio DNS y uso de _hosting_ web](practica-9)           | _Nube pública_ | _pendiente_
| **Práctica 10**: [Creación de recursos en Azure](practica-10)                           | _Nube pública_ | _pendiente_
| **Práctica 11**: [Implementación de sitios web sobre HTTPS](practica-11)                | _Nube pública_ | _pendiente_
