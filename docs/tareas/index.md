---
title: Tareas
---

# Tareas

| Título                                                     | Ambiente      | Entrega
|:-----------------------------------------------------------|:-------------:|:----------:|
| **Tarea 1**: [Curso de nivelación de GNU/Linux](tarea-1)   | GNU/Linux     | 2022-08-24
| **Tarea 2**: [Instalación de Cisco Packet Tracer](tarea-2) | Packet Tracer | 2022-08-24
| **Tarea 3**: [Flujo de trabajo en GitLab](tarea-3)         | GitLab        | 2022-08-29
| **Tarea 4**: [Laboratorio virtual de redes](tarea-4)       | VirtualBox    | _pendiente_
| **Tarea 5**: [Captura de tráfico de red](tarea-5)          | WireShark     | _pendiente_
